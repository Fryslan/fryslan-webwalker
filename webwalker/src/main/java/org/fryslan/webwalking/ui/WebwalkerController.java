package org.fryslan.webwalking.ui;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ZoomEvent;
import org.fryslan.webwalking.WebWalker;

import java.awt.image.BufferedImage;

public class WebwalkerController {

    @FXML
    private static ImageView mainImageView;

    @FXML
    void keyTyped(KeyEvent event) {

    }

    @FXML
    void moueClicked(MouseEvent event) {
        System.out.println("Clicked imageview");
    }

    @FXML
    void mouseMoved(MouseEvent event) {

    }

    @FXML
    void zoom(ZoomEvent event) {

    }


    @FXML
    void initialize() {
        System.out.println(mainImageView.getX());
        WebWalker.loadedUI = true;
    }


    public static void setImage(BufferedImage image){
        Image i = SwingFXUtils.toFXImage(image, null);
        System.out.println((image == null) + " | "+ (mainImageView == null) );
        mainImageView.setImage(i);
    }

}
