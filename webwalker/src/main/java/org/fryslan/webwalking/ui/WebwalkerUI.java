package org.fryslan.webwalking.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class WebwalkerUI extends Application {
    public static Stage stage;

//    public static void main(String[] args) {
//        launch(args);
//    }

    public static void initilize() {
        new JFXPanel();

        Platform.runLater(() -> {
            Platform.setImplicitExit(false);
            try {
                System.out.println("1");
                new WebwalkerUI().start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(new URL("https://gitlab.com/Fryslan/fryslan-webwalker/-/raw/main/webwalker/src/main/java/org/fryslan/webwalking/ui/Webwalker.fxml"));
        loader.setController(new WebwalkerController());

        Parent page = loader.load();
        Scene scene = new Scene(page);

        stage.setScene(scene);
        stage.show();

    }

//    @Override
//    public void start(Stage stage) {
//        stage.show();
//    }
}
