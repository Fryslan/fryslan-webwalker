package org.fryslan.webwalking;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import org.fryslan.webwalking.ui.WebwalkerController;
import org.fryslan.webwalking.ui.WebwalkerUI;
import org.powerbot.script.Condition;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Script;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;
import z.G;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@Script.Manifest(name = "Webwalker", description = "Webwalking")
public class WebWalker extends PollingScript<ClientContext> {

    public static boolean loadedUI;
    private final int LOADING_STATE = 25;
    private final int MAP_X_OFFSET = 1048;
    private final int MAP_Y_OFFSET = 3292;
    private final int TILE_X_OFFSET = 1152;
    private final int TILE_Y_OFFSET = 2496;
    private final String DIRECTORY = "/Walker/Web/";
    private int cachedGameState = 0;
    private ArrayList<TileDataItem> tileDataItems = new ArrayList<>();

    @Override
    public void poll() {

        int mapX = (ctx.game.mapOffset().x() - MAP_X_OFFSET) / 104;
        int mapY = (ctx.game.mapOffset().y() - MAP_Y_OFFSET) / 104;
        String fileName = mapX + "-" + mapY + ".json";

        if (ctx.game.clientState() != cachedGameState) {
            if (ctx.game.clientState() == LOADING_STATE) {

                for (int i = 0; i < 104; i++) {
                    for (int j = 0; j < 104; j++) {
                        Tile base = ctx.game.mapOffset();
                        Tile tile = new Tile(base.x()+i, base.y()+j, base.floor());
                        tileDataItems.add(new TileDataItem(tile.x(),tile.y(),tile.floor(), tile.collisionFlag(ctx.client().getCollisionMaps()[tile.floor()].getFlags())));
                    }
                }

                try {
                    saveMapData(fileName, tileDataItems);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                refreshImage();
            }
            cachedGameState = ctx.game.clientState();
        }

    }

    @Override
    public void start() {

        System.out.println("Loading ui");
        WebwalkerUI.initilize();
        System.out.println("Loaded ui");

        Condition.wait(() -> loadedUI, 30, 100);
        LoadMapData();
        if(tileDataItems.size() > 0) {
            refreshImage();
        }

        super.start();
    }

    @Override
    public void stop() {
        super.stop();
    }

    private void LoadMapData() {

        File dir = new File(DIRECTORY);
        if(!dir.exists()){
            dir.mkdirs();
        }
        File[] directoryListing = dir.listFiles();
        if(directoryListing.length > 0) {
            for (File file : directoryListing) {
                if (file.exists()) {
                    Gson gson = new Gson();
                    JsonParser jsonParser = new JsonParser();
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(file));
                        JsonElement jsonElement = jsonParser.parse(br);

                        //Create generic type
                        Type type = new TypeToken<List<TileDataItem>>() {
                        }.getType();
                        ArrayList<TileDataItem> items = gson.fromJson(jsonElement, type);
                        for (TileDataItem item : items) {
                            tileDataItems.add(item);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void refreshImage() {

        int width = 5000;
        int height = 8000;
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        for (TileDataItem item : tileDataItems) {
            int x = item.getX() - TILE_X_OFFSET;
            int y = item.getY() - TILE_Y_OFFSET;
            int flag = item.getFlag();
            int gray = (255 & 0xff) << 24 | (153 & 0xff) << 16 | (153 & 0xff) << 8 | (153 & 0xff);
            int red = (255 & 0xff) << 24 | (255 & 0xff) << 16 | (0 & 0xff) << 8 | (0 & 0xff);

            if(flag == 0) {
                System.out.println("Trying to write "+x+" - "+y);
                image.setRGB(x, y, gray);
            }else{
                image.setRGB(x, y, red);
            }
        }
        //write image
        try{
            File f = new File(DIRECTORY+"test.jpg");
            ImageIO.write(image, "jpg", f);
        }catch(IOException e){
            System.out.println("Error: " + e);
        }
        System.out.println("Setting image");
        WebwalkerController.setImage(image);
    }

    private void saveMapData(String fileName, ArrayList<TileDataItem> data) throws IOException {

        Gson gson = new Gson();
        String s = gson.toJson(data);
        String path = DIRECTORY + fileName;

        File myFile = new File(path);
        System.out.println("saved data to: "+myFile.getAbsolutePath());
        myFile.createNewFile();
        FileOutputStream fOut = new FileOutputStream(myFile);
        OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
        myOutWriter.append(s);
        myOutWriter.close();
        fOut.close();
    }

}
