package org.fryslan.webwalking;

public class TileDataItem {
    private final int x;
    private final int y;
    private final int z;
    private final int f;

    public TileDataItem(int x, int y, int z, int f){
        this.x = x;
        this.y = y;
        this.z = z;
        this.f = f;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public int getFlag() {
        return f;
    }

}
